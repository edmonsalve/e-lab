export const environment = {
    production: false,
    apiUrl: "http://localhost:3000/api",
    firebase: {
        apiKey: "AIzaSyC3YLJh-_S9_t5gS7K6XmljrlAikOigLtk",
        authDomain: "ede-lab.firebaseapp.com",
        databaseURL: "https://ede-lab.firebaseio.com",
        projectId: "ede-lab",
        storageBucket: "ede-lab.appspot.com",
        messagingSenderId: "213082012679"
    }
};
