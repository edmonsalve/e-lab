export const environment = {
    production: true,
    apiUrl: "https://edance-lab.com/api",
    firebase: {
      apiKey: "AIzaSyC3YLJh-_S9_t5gS7K6XmljrlAikOigLtk",
      authDomain: "ede-lab.firebaseapp.com",
      databaseURL: "https://ede-lab.firebaseio.com",
      projectId: "ede-lab",
      storageBucket: "ede-lab.appspot.com",
      messagingSenderId: "213082012679"
    }
};
