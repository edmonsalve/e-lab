import { Component, OnInit, OnDestroy } from '@angular/core';
import User from '../models/user.model';
import { AuthService } from '../services/auth/auth.service';
import { EventService } from '../services/event/event.service';
import EDEevent from '../models/event.model';
import { ExcelGeneratorService } from '../services/filesGenerators/excelGenerator.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-event-statistics',
  templateUrl: './event-statistics.component.html',
  styleUrls: ['./event-statistics.component.scss']
})
export class EventStatisticsComponent implements OnInit {

  public loggedUser: User;
  public totalEarned: number = 0;
  public totalEarnedEDE: number = 0;
  public totalEarnedUser: number = 0;


  public eventTickets: any[] = [];
  public paymentStages: any[] = [];
  public paymentMethods: any[] = [];
  public eventToShow: EDEevent;
  public totalCheckedInTickets: number = 0;

  constructor(
    private _authService: AuthService,
    private _eventsService: EventService,
    private _excelGeneratorService: ExcelGeneratorService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.loggedUser = this._authService.getLoggedUser();
    this.eventToShow = this._eventsService.getSelectedEvent();
    if (!this.eventToShow) {
      const eventId = this._activatedRoute.snapshot.paramMap.get('id');

      this._eventsService.getEvent(eventId).subscribe((event: any) => {
        this.eventToShow = event.data[0];
      });
    }
    if (this.eventToShow.eventType == "free") {
      this.getFreeEventStatistics(this.eventToShow._id);
    } else {
      this.getEventStatistics(this.eventToShow._id);
    }
  }

  public logout() {
    this._authService.loggout();
    this._router.navigate(['/']);
  }

  public goToEvents() {
    this._router.navigate(['/events']);
  }

  public goToUsers() {
    this._router.navigate(['/users']);
  }

  public goToHome() {
    this._router.navigate(['/home']);
  }

  private getFreeEventStatistics(eventId: string) {
    this._eventsService.getFreeStatistics(eventId).subscribe((statistics: any) => {
      this.eventTickets = statistics.data;
      this.getTotalCheckedIn();
    });
  }

  private getEventStatistics(eventId: string) {
    this._eventsService.getStatistics(eventId).subscribe((statistics: any) => {
      this.eventTickets = statistics.data;
      this.paymentStages = statistics.resumedPaymentsData.paymentStages;
      this.paymentMethods = statistics.resumedPaymentsData.paymentMethods;
      this.getTotalEarned();
      this.getTotalCheckedIn();
    });
  }

  private getTotalEarned() {
    this.eventTickets.map((ticket) => {
      this.totalEarned = this.totalEarned + ticket.payment.value;
      if (ticket.payment.value < 27500) {
        // Le restamos el ticket service
        this.totalEarned = this.totalEarned - 2500;
        if (ticket.payment.entity == "pse") {
          this.totalEarnedEDE = this.totalEarnedEDE + 800;
        } else {
          this.totalEarnedEDE = this.totalEarnedEDE + 1100;
        }
        this.totalEarnedEDE = this.totalEarnedEDE + (ticket.payment.value * 0.04);
      } else {
        // Le restamos el ticket service
        this.totalEarned = this.totalEarned - 4500;
        this.totalEarnedEDE = this.totalEarnedEDE + ((ticket.payment.value - 4500) * 0.05);
        if (ticket.payment.entity == "PSE") {
          this.totalEarnedEDE = this.totalEarnedEDE + 800;
        } else {
          this.totalEarnedEDE = this.totalEarnedEDE + 1500;
        }
      }
      
      this.totalEarnedUser = this.totalEarned - this.totalEarnedEDE;
    });
  }

  private getTotalCheckedIn() {
    this.eventTickets.map(elm => {
      if (elm.status == 'Redimido') {
        this.totalCheckedInTickets = this.totalCheckedInTickets + 1;
      }
    });
  }

  public downloadGuestlist() {
    this._excelGeneratorService.exportAsExcelFile(this.eventToShow.guestlistUsers, `${this.eventToShow.name.substr(0,10)}_usuarios_guestlist_${new Date().toLocaleDateString()}`);
  }

}
