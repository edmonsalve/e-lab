export default class Subcategory {
    public name: string;
    public parentId: string;
    public imageUrl: string;

    constructor() {
        this.name = '';
        this.parentId = '';
        this.imageUrl = '';
    }
}