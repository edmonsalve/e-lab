export default class Category {
    public _id?: string;
    public name: string;
    public description: string;
    public imageSrc: string;
    public subcategories?: Array<any>;

    constructor() {
        this.name = '';
        this.description = '';
        this.imageSrc = '';
    }
}