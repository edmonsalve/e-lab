export default class UserFilters {
    public name: string;
    public status: number;
    public city: string;
    public role: string;
    public signature: string;

    constructor() {
        this.name = "";
        this.status = 3;
        this.signature = "";
        this.city = "";
        this.role = "";
    }
}