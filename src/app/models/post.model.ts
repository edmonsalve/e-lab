export default class Post {
    public title: string;
    public description: string;
    public imageSrc: string;

    constructor() {
        this.title = "";
        this.description = "";
        this.imageSrc = "";
    }
}