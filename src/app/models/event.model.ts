import Ticketing from "./ticketing.model";
export default class EDEevent {
    public _id: string;
    public userId: string;
    public name: string;
    public musicalGender: string;
    public description: string;
    public hasVj: string;
    public vjLineUp: string;
    public lineUp: string;
    public imageSrc: string;
    public location: { city: string, name: string, address: string };
    public eventType: string;
    public tickets: Ticketing[] | 0;
    public guestlist: number;
    public guestlistQuantity: number;
    public guestlistUsers: Array<{ cedula: number, nombre: string }>;
    public startTime: string;
    public endTime: string;
    public startDate: Date;
    public endDate: Date;
    public benefits: string;
    public warnings: string;
    public status: string;
    public signature: string;
    public createdDate: string;

    constructor() {
        this.name = '';
        this.musicalGender = '';
        this.eventType = '';
        this.description = '';
        this.lineUp = '';
        this.imageSrc = '';
        this.location = {
            city: '',
            address: '',
            name: ''
        };
        this.guestlistUsers = [];
        this.startDate = new Date();
        this.endDate = new Date();
        this.startTime = "21:00";
        this.endTime = "04:00";
        this.tickets = 0;
        this.guestlist = null;
        this.guestlistQuantity = 0;
        this.benefits = '';
        this.warnings = '';
        this.status = 'inactive';
        this.signature = '';
    }
}
