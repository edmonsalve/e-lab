export default class User {
    public _id: string;
    public name: string;
    public lastname: string;
    public password: string;
    public signature: string;
    public age: string;
    public gender: string;
    public location: string;
    public cellPhone: string;
    public expYears: string;
    public eventsLastYear: string;
    public clubOrOrganizer: string;
    public socialLink1: string;
    public socialLink2: string;
    public email: string;
    public role: string;
    public eventId: string;
    public status: number;
    public created_at: string;

    constructor() {
        this.name="";
        this.lastname="";
        this.location="";
        this.cellPhone="";
        this.expYears="";
        this.eventsLastYear="";
        this.clubOrOrganizer = "";
        this.socialLink1 = "";
        this.socialLink2 = "";
        this.eventId = null;
        this.age="";
        this.gender="";
        this.email="";
        this.status=0;
    }
}