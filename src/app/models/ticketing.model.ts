export default class Ticketing {
    public name: string;
    public start_date: string;
    public end_date: string;
    public amount_available: number;
    public amount_sold: number;
    public price: number;
}