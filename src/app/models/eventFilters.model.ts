export default class EventFilters {
    public title: string;
    public createdAt: Date;
    public startDate: Date;
    public state: string;
    public signature: string;

    constructor() {
        this.title = "";
        this.state = "";
        this.signature = "";
    }
}