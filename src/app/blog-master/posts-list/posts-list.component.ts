import { Component, OnInit } from '@angular/core';

import Post from 'src/app/models/post.model';

@Component({
  selector: 'posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.scss']
})
export class PostsListComponent implements OnInit {

  public title: string = "Entrevistas";
  public posts: Post[] = [
    {
      "title": "Conoce al artista",
      "description": "Entrevistamos a varios artistas FREEDOM locales, dale un vistazo",
      "imageSrc": "https://i.ytimg.com/vi/-ZEhl2z2ZPk/hqdefault.jpg"
    },
    {
      "title": "Conoce al artista",
      "description": "Entrevistamos a varios artistas FREEDOM locales, dale un vistazo",
      "imageSrc": "https://i.ytimg.com/vi/-ZEhl2z2ZPk/hqdefault.jpg"
    },
    {
      "title": "Conoce al artista",
      "description": "Entrevistamos a varios artistas FREEDOM locales, dale un vistazo",
      "imageSrc": "https://i.ytimg.com/vi/-ZEhl2z2ZPk/hqdefault.jpg"
    },
    {
      "title": "Conoce al artista",
      "description": "Entrevistamos a varios artistas FREEDOM locales, dale un vistazo",
      "imageSrc": "https://i.ytimg.com/vi/-ZEhl2z2ZPk/hqdefault.jpg"
    },
    {
      "title": "Conoce al artista",
      "description": "Entrevistamos a varios artistas FREEDOM locales, dale un vistazo",
      "imageSrc": "https://i.ytimg.com/vi/-ZEhl2z2ZPk/hqdefault.jpg"
    },
    {
      "title": "Conoce al artista",
      "description": "Entrevistamos a varios artistas FREEDOM locales, dale un vistazo",
      "imageSrc": "https://i.ytimg.com/vi/-ZEhl2z2ZPk/hqdefault.jpg"
    },
    {
      "title": "Conoce al artista",
      "description": "Entrevistamos a varios artistas FREEDOM locales, dale un vistazo",
      "imageSrc": "https://i.ytimg.com/vi/-ZEhl2z2ZPk/hqdefault.jpg"
    },
    {
      "title": "Conoce al artista",
      "description": "Entrevistamos a varios artistas FREEDOM locales, dale un vistazo",
      "imageSrc": "https://i.ytimg.com/vi/-ZEhl2z2ZPk/hqdefault.jpg"
    },
    {
      "title": "Conoce al artista",
      "description": "Entrevistamos a varios artistas FREEDOM locales, dale un vistazo",
      "imageSrc": "https://i.ytimg.com/vi/-ZEhl2z2ZPk/hqdefault.jpg"
    },
    {
      "title": "Conoce al artista",
      "description": "Entrevistamos a varios artistas FREEDOM locales, dale un vistazo",
      "imageSrc": "https://i.ytimg.com/vi/-ZEhl2z2ZPk/hqdefault.jpg"
    },
    {
      "title": "Conoce al artista",
      "description": "Entrevistamos a varios artistas FREEDOM locales, dale un vistazo",
      "imageSrc": "https://i.ytimg.com/vi/-ZEhl2z2ZPk/hqdefault.jpg"
    },
    {
      "title": "Conoce al artista",
      "description": "Entrevistamos a varios artistas FREEDOM locales, dale un vistazo",
      "imageSrc": "https://i.ytimg.com/vi/-ZEhl2z2ZPk/hqdefault.jpg"
    },
    {
      "title": "Conoce al artista",
      "description": "Entrevistamos a varios artistas FREEDOM locales, dale un vistazo",
      "imageSrc": "https://i.ytimg.com/vi/-ZEhl2z2ZPk/hqdefault.jpg"
    },
    {
      "title": "Conoce al artista",
      "description": "Entrevistamos a varios artistas FREEDOM locales, dale un vistazo",
      "imageSrc": "https://i.ytimg.com/vi/-ZEhl2z2ZPk/hqdefault.jpg"
    },
    {
      "title": "Conoce al artista",
      "description": "Entrevistamos a varios artistas FREEDOM locales, dale un vistazo",
      "imageSrc": "https://i.ytimg.com/vi/-ZEhl2z2ZPk/hqdefault.jpg"
    },
    {
      "title": "Conoce al artista",
      "description": "Entrevistamos a varios artistas FREEDOM locales, dale un vistazo",
      "imageSrc": "https://i.ytimg.com/vi/-ZEhl2z2ZPk/hqdefault.jpg"
    }
  ];

  constructor() { }

  ngOnInit() {
  
  }

}
