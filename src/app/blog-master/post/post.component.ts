import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  @Input() title: string = '';
  @Input() description: string = '';
  @Input() imageSrc: string = '';

  constructor() { }

  ngOnInit() {

  }

  

}
