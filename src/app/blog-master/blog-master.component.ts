import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

import { BlogService } from '../services/blog/blog.service';
import Category from '../models/category.model';

@Component({
  selector: 'app-blog-master',
  templateUrl: './blog-master.component.html',
  styleUrls: ['./blog-master.component.scss'],
})
export class BlogMasterComponent implements OnInit {
  
  public currentLocation: string = null;
  public currentTitle = 'Categorias principales';
  public categories: Category[] = [];

  public showLoader: boolean = false;
  public loaderMessage: string = "";

  constructor(
    private blogService: BlogService,
    private _router: Router,
    private _dialog: MatDialog
    ) { }

  ngOnInit() {
    this.getBlogCategories();
  }

  private getBlogCategories() {
    this.blogService.getMasterCategories().then((categories: Category[]) => {
      this.categories = categories;
    }).catch((error: any) => {
      throw(error);
    });
  }

  public getSubcategories(parent: Category) {
    if (parent.subcategories) {
      this.currentLocation = `${this.currentLocation}>${parent.name}`;
      this.currentTitle = parent.name;
      this.categories = parent.subcategories;
    } else {
      this.getPosts(parent, this.currentLocation);
    }
  }

  private getPosts(parent: Category, location: string) {
    this._router.navigate(['/posts']);
  }

  public createCategorie() {
    const dialogRef = this._dialog.open(CreateCategoryDialog, {
      width: '400px',
      data: { name: 'Animal' }
    });

    const onAddSub = dialogRef.componentInstance.onAddNewCategory.subscribe((data) => {
      dialogRef.close();
      this.loaderMessage = "Creando la categoria";
      this.showLoader = true;
      this.blogService.createCategory(data).subscribe((response) => {
        this.categories.push(response['record']);
        setTimeout(() => {
          this.showLoader = false;
        }, 2000)
      });
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }

  

}

@Component({
  selector: 'create-category-dialog',
  templateUrl: 'create-category-dialog.html',
  styleUrls: ['create-category-dialog.scss']
})
export class CreateCategoryDialog {

  public onAddNewCategory = new EventEmitter();
  public name: string = "";
  public description: string = "";
  public imageSrc: string = "";

  constructor(
    public dialogRef: MatDialogRef<CreateCategoryDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}


  onConfirmClick() {
    let dataToEmmit = { name: this.name, description: this.description, imageSrc: this.imageSrc };
    this.onAddNewCategory.emit(dataToEmmit);
  }

  onNoClick() {
    this.dialogRef.close();
  }
}