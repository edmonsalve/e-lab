import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import User from '../models/user.model';
import { AuthService } from '../services/auth/auth.service';
import { UserService } from '../services/user/user.service';
import { SwalComponent } from '@toverux/ngx-sweetalert2';

@Component({
  selector: 'app-user-validation',
  templateUrl: './user-validation.component.html',
  styleUrls: ['./user-validation.component.scss']
})
export class UserValidationComponent implements OnInit {

  public loggedUser: User;
  public userToVerify: User = new User();
  public userId: string;
  public userStatus:  number = 0;

  public showLoader: boolean;
  public alreadyApproved: boolean = false;

  @ViewChild('confirmationApprov') confirmationSwal: SwalComponent;
  @ViewChild('confirmationReject') confirmationSwalRejected: SwalComponent;
  @ViewChild('errorAccessType') errorAccessTypeSwal: SwalComponent;

  constructor(
    private _activeRoute: ActivatedRoute,
    private _authService: AuthService,
    private _userService: UserService,
    private _router: Router
  ) { }

  ngOnInit() {
    this.loggedUser = this._authService.getLoggedUser();
    this._activeRoute.params.subscribe((params) => {
      this.userId = params['id'];
      this.getUser();
    });
  }

  private async getUser() {
    this._userService.getUser(this.userId).subscribe((response: any) => {
      this.userToVerify = response.user;
      if (this.userToVerify.status == 2 || this.userToVerify.status == 0) {
        this.alreadyApproved = true;
      }
    });
  }

  public approvUser(validated: boolean) {
    if (this.userStatus != 0) {
      this.showLoader = true;
      this._userService.validateUser(this.userId, validated, this.userStatus).subscribe((response: any) => {
        this.showLoader = false;
        if (validated == true) {
          this.confirmationSwal.show();
        } else {
          this.confirmationSwalRejected.show();
        }
      });
    } else {
      this.errorAccessTypeSwal.show();
    }
  }

  public goToHome() {
    this._router.navigate(['/home']);
  }

}
