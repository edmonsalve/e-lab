import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppRootComponent } from './app-root/app-root.component';
import { CreateEventComponent } from './create-event/create-event.component';
import { UsersPanelComponent, CreateUserDialog } from './users-panel/users-panel.component';
import { environment } from 'src/environments/environment';
import { UsersSignupComponent, TermsAndConditionsDialog } from './users-signup/users-signup.component';

import { AngularEditorModule } from '@kolkov/angular-editor';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import { EventsComponent } from './events/events.component';
import { LoginComponent } from './login/login.component';
import { CustomErrorHandlerService } from './error-handler/custom-error-handler.service';
import { LoaderComponent } from './loader/loader.component';
import { BlogMasterComponent, CreateCategoryDialog } from './blog-master/blog-master.component';
import { CategoryItemComponent } from './blog-master/category-item/category-item.component';
import { PostsListComponent } from './blog-master/posts-list/posts-list.component';
import { PostComponent } from './blog-master/post/post.component';

import { AngularFireModule } from "@angular/fire";
import { AngularFireStorageModule } from "@angular/fire/storage";
import { NgxCurrencyModule } from "ngx-currency";
import { AmazingTimePickerModule } from 'amazing-time-picker';

import { 
  MatDialogModule, 
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatOptionModule,
  MatSelectModule,
  MatDatepicker,
  MatDatepickerModule,
  MatNativeDateModule
} from "@angular/material";

import { SweetAlert2Module } from "@toverux/ngx-sweetalert2";
import { EventDetailComponent } from './event-detail/event-detail.component';
import { EventStatisticsComponent } from './event-statistics/event-statistics.component';
import { UserValidationComponent } from './user-validation/user-validation.component';
import { UsersRecoveryPasswordComponent } from './users-recovery-password/users-recovery-password.component';
import { UsersChangePasswordComponent } from './users-change-password/users-change-password.component'
import { JwtInterceptorService } from './services/auth/jwt-interceptor.service';
import { PaymentStatusComponent } from './payment-status/payment-status.component';
import { OrientesCrowdComponent } from './orientes-crowd/orientes-crowd.component';
import { ElectronicBusinessMeetingComponent } from './electronic-business-meeting/electronic-business-meeting.component';

import { PaginatorComponent } from './paginator/paginator.component';
@NgModule({
  declarations: [
    AppComponent,
    AppRootComponent,
    CreateEventComponent,
    EventsComponent,
    LoginComponent,
    LoaderComponent,
    CreateCategoryDialog,
    CreateUserDialog,
    TermsAndConditionsDialog,
    BlogMasterComponent,
    CategoryItemComponent,
    PostsListComponent,
    PostComponent,
    UsersPanelComponent,
    UsersSignupComponent,
    EventDetailComponent,
    EventStatisticsComponent,
    UserValidationComponent,
    UsersRecoveryPasswordComponent,
    UsersChangePasswordComponent,
    PaymentStatusComponent,
    OrientesCrowdComponent,
    ElectronicBusinessMeetingComponent,
    PaginatorComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    AngularEditorModule,
    AngularDateTimePickerModule,
    MatDialogModule,
    MatNativeDateModule,
    AmazingTimePickerModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatOptionModule,
    MatSelectModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireStorageModule,
    SweetAlert2Module.forRoot(),
    ReactiveFormsModule,
    NgxCurrencyModule
  ],
  entryComponents: [
    CreateCategoryDialog,
    CreateUserDialog,
    TermsAndConditionsDialog
  ],
  providers: [
    CustomErrorHandlerService,
    { provide: ErrorHandler, useClass: CustomErrorHandlerService },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
