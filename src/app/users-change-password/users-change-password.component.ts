import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../services/user/user.service';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import { AuthService } from '../services/auth/auth.service';

@Component({
  selector: 'app-users-change-password',
  templateUrl: './users-change-password.component.html',
  styleUrls: ['./users-change-password.component.scss']
})
export class UsersChangePasswordComponent implements OnInit {

  public swalErrorTitle: string;
  public isChangeMode: boolean = false;
  public showLoader: boolean = false;

  public currentPassword: string;
  public newPassword: string;
  public passwordConfirmation: string;
  public userId: string;

  @ViewChild('confirmation') confirmationSwal: SwalComponent;
  @ViewChild('emptyFieldsError') emptyFieldsError: SwalComponent;
  @ViewChild('passwordMatchError') passwordMatchError: SwalComponent;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _authService: AuthService
  ) { }

  ngOnInit() {
    this._route.queryParams.subscribe((params) => {
      if (!params.userId && !params.token) {
        this.isChangeMode = true;
        this.userId = this._authService.getLoggedUser()._id;
      } else {
        this.userId = params.userId;
      }
    });
  }

  public changePassword() {
    if (this.validateForm()) {
      this.showLoader = true;
      let payload = { 
        isChangeMode: this.isChangeMode, 
        currentPassword: this.currentPassword, 
        newPassword: this.newPassword,
        userId: this.userId
      }
      this._userService.changePassword(payload).subscribe((response: any) => {
        if (response.status == "Password changed") {
          this.showLoader = false;
          this.confirmationSwal.show();
        }
      }, (error) => {
        console.log(error);
        this.passwordMatchError.show();
      });
    }
  }

  private validateForm(): boolean {
    if (this.isChangeMode) {
      if (this.currentPassword && this.newPassword && this.passwordConfirmation) {
        if (this.newPassword == this.passwordConfirmation) {
          return true;
        } else {
          this.passwordMatchError.show();
          return false;
        }
      } else {
        this.emptyFieldsError.show();
        return false;
      }
    } else if (this.passwordConfirmation && this.newPassword) {
      if (this.newPassword == this.passwordConfirmation) {
        return true;
      }
    } else {
      this.passwordMatchError.show();
      return false;
    }
  }

  public goToLogin() {
    this._router.navigate(['/']);
  }

}
