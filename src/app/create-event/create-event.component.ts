import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import EDEevent from '../models/event.model';
import * as moment from 'moment';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import FileUpload from '../models/fileUpload.model';
import Ticketing from '../models/ticketing.model';
import { EventService } from '../services/event/event.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/auth/auth.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import { MatDatepicker } from '@angular/material';
import User from '../models/user.model';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.scss']
})
export class CreateEventComponent implements OnInit {

  ticketBatchesDates = [{price: ''}, {price: ''}, {price: ''}, {price: ''}, {price: ''}];

  public musicalGenders: string[] = [
      'House',
      'Minimal',
      'Techno',
      'Tech House',
      'Acid',
      'Ambient',
      'Trance',
      'Drum n bass',
      'Festival',
      'Deep house',
      'Dubstep',
      'Hardstyle',
      'Progressive House',
      'Progressive Trance',
      'Electro'
  ];

  public customCurrencyMaskConfig = {
    align: "left",
    allowNegative: false,
    allowZero: true,
    decimal: ",",
    precision: 0,
    prefix: "$ ",
    suffix: "",
    thousands: ".",
    nullable: false
};

  public showLoader: boolean = false;
  public loaderMessage: string = "";

  startDate: Date = new Date();
  endDate: Date = new Date();
  public guestlist: string = "No";
  public datetimePickerConfig = {
    bigBanner: true,
    timePicker: true,
    format: 'dd-MM-yyyy hh:mm',
    defaultOpen: false
  };
  public datetimePickerConfigTickets = {
    bigBanner: true,
    timePicker: false,
    format: 'dd-MM-yyyy',
    defaultOpen: false
  };

  public haveAnImage: boolean = false;
  public imageToUpload: FileUpload = null;
  private imageWasChanged: boolean = false;
  public eventToSave: EDEevent = new EDEevent();
  public ticketsBatches: number = 1;

  public temporalImage: string = "assets/imgs/no-image.png";

  public eventToSaveFormOne: FormGroup;
  public eventToSaveFormTwo: FormGroup;

  public isEditMode: boolean = false;

  public loggedUser: User;

  @ViewChild('fileManager') fileManager: ElementRef;
  @ViewChild('errorSwal') errorSwal: SwalComponent;
  @ViewChild('errorSwalImg') errorSwalImg: SwalComponent;
  @ViewChild('confirmation') confirmationSwal: SwalComponent;
  @ViewChild('errorTicketingData') errorTicketingData: SwalComponent;
  @ViewChild('errorTicketingDates') errorTicketingDates: SwalComponent;

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _afStorage: AngularFireStorage,
    private _eventService: EventService,
    private _authService: AuthService,
    private _fb: FormBuilder
  ) {
    this.buildFormValidators();
  }

  ngOnInit() {
    moment.locale('es');
    this.loggedUser = this._authService.getLoggedUser();
    this.musicalGenders.sort();
    this._activatedRoute.url.subscribe((url) => {
      if (url[0].path == 'update-event') {
        this.isEditMode = true;
        this.fillEventDataToEdit();
      }
    })
  }

  public onDateSelected(data: Date) {
  }

  public async saveEvent(publish: boolean = false) {
    if (!this.isValidEvent()) {
      this.errorSwal.show();
    } else {
      this.fillEventData();
      this.loaderMessage = "Estamos configurando tu evento...";
      this.eventToSave.createdDate = new Date().toString();
      this.eventToSave.status = 'active';
      this.eventToSave.signature = this.loggedUser.signature;
      this.eventToSave.userId = this.loggedUser._id;
      if (this.eventToSave.eventType == "payment") {
        this.eventToSave.tickets = this.resolveTicketing();
        let validTicketing = await this.validateTicketing(this.eventToSave.tickets);
        if (validTicketing == false) {
          return;
        }
      } else {
        this.eventToSave.tickets = null;
      }
      this.showLoader = true;
      this.eventToSave.imageSrc = await this.uploadImage(this.imageToUpload);
      this._eventService.saveEvent(this.eventToSave).subscribe((response) => {
        this.eventToSaveFormOne.reset();
        this.eventToSaveFormTwo.reset();
        this.confirmationSwal.show();
        this.eventToSave = new EDEevent();
        this.showLoader = false;
        this._router.navigate(['/events']);
      }, (error) => {
        this.errorSwal.text = "No pudimos conectar con el servidor, intenta de nuevo mas tarde.";
        this.errorSwal.show();
        throw(error);
      });
    }
  }

  public async updateEvent() {
    if (!this.isValidEvent()) {
      this.errorSwal.show();
    } else {
      this.loaderMessage = "Estamos configurando tu evento...";
      this.showLoader = true;
      this.fillEventData();
      if (this.imageWasChanged) {
        this.eventToSave.imageSrc = await this.uploadImage(this.imageToUpload);
      }
      this._eventService.updateEvent(this.eventToSave).subscribe((response) => {
        this.eventToSaveFormOne.reset();
        this.eventToSaveFormTwo.reset();
        this.confirmationSwal.show();
        this.eventToSave = new EDEevent();
        this.showLoader = false;
        this._router.navigate(['/events']);
      }, (error) => {
        this.errorSwal.text = "No pudimos conectar con el servidor, intenta de nuevo mas tarde.";
        this.errorSwal.show();
        throw(error);
      });
    }
  }

  public resolveTicketing() {
    let names = document.querySelectorAll('.batchName');
    let prices = document.querySelectorAll('.batchPrice');
    let quantities = document.querySelectorAll('.batchQuantity');
    let ticketingStages: Ticketing[] = [];
    for (let i=0; i < names.length; i++) {
      let ticketing = new Ticketing();
      ticketing.name = names[i]['value'];
      ticketing.price = Number(prices[i]['value'].replace("$", "").replace(".", "").trim());
      ticketing.amount_available = Number(quantities[i]['value']);
      ticketing.start_date = this.ticketBatchesDates[i+1]['start'];
      ticketing.end_date = this.ticketBatchesDates[i+1]['end'];
      ticketing.amount_sold = 0;
      ticketingStages.push(ticketing);
    }
    return ticketingStages;
  }

  public validateTicketing(ticketingStages: Ticketing[]): Promise<boolean> {
    return new Promise((resolve, reject) => {
      let isValid = false;
      if (ticketingStages.length == 1 && ticketingStages[0].start_date == undefined && ticketingStages[0].end_date == undefined) {
        this.errorTicketingData.show();
        resolve(isValid);
      } else {
        ticketingStages.map((stage, index) => {
          if (stage.name && stage.price && stage.start_date && stage.amount_available && stage.end_date) {
            if (moment(stage.start_date).isBefore(this.eventToSave.startDate) && moment(stage.start_date).isBefore(stage.end_date)) {
              if (ticketingStages[index+1] && ticketingStages[index+1].price && ticketingStages[index+1].start_date) {
                if (moment(ticketingStages[index+1].start_date).isAfter(stage.end_date) && moment(ticketingStages[index+1].end_date).isAfter(stage.end_date)) {
                  isValid = true;
                } else {
                  this.errorTicketingDates.show();
                  isValid = false;
                  resolve(isValid);
                }
              } else {
                isValid = true;
              }
            } else {
              this.errorTicketingDates.show();
              isValid = false;
              resolve(isValid);
            }
          } else {
            this.errorTicketingData.show();
            isValid = false;
            resolve(isValid);
          }
        });
        resolve(isValid);
      }
    })
  }

  public openFileChooser() {
    let elm = this.fileManager.nativeElement as HTMLElement
    elm.click();
  }

  public onFileSelected(event) {
    if (this.isEditMode) {
      this.imageWasChanged = true;
    }
    const file = event.target.files.item(0);
    if (file.type.match('image.*')) {
      this.imageToUpload = new FileUpload(file);
      let reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (e: ProgressEvent) => {
        this.temporalImage = String((<FileReader>e.target).result);
        this.haveAnImage = true;
      }
    }
  }

  public uploadImage(fileUpload: FileUpload): Promise<string> {
    return new Promise((resolve, reject) => {
      const path = `test/${new Date().getTime()}_${fileUpload.file.name}`;
      this._afStorage.upload(path, fileUpload.file).then((snapshot) => {
        const ref = this._afStorage.ref(path);
        ref.getDownloadURL().subscribe(url => {
          resolve(url);
        });
      });
    })
  }

  public createRange(number){
    let items: number[] = [];
    for(let i = 1; i <= number; i++){
       items.push(i);
    }
    return items;
  }

  public buildFormValidators() {
    this.eventToSaveFormOne = this._fb.group({
      name: ['', Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(45)
      ])],
      musicalGender: ['', Validators.required],
      description: ['', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(450)
      ])],
      lineUp: ['', Validators.compose([
        Validators.required,
        Validators.minLength(2)
      ])],
      vjLineUp: [''],
      hasvj: ['', Validators.required]
    }, {
      validator: (group) => {
        if (group.controls.hasvj.value == '1') {
          return Validators.required(group.controls.vjLineUp)
        }
      }
    });
    this.eventToSaveFormTwo = this._fb.group({
      city: ['', Validators.required],
      address: ['', Validators.required],
      guestlist: ['No', Validators.required],
      guestlistQuantity: [''],
      benefits: [''],
      warnings: ['',]
    }, { validator: (group) => {
      if (group.controls.guestlist.value == '1') {
        return Validators.required(group.controls.guestlistQuantity)
      }
    }});
    
  }

  public isValidEvent(): boolean {
    this.eventToSaveFormOne.updateValueAndValidity();
    this.eventToSaveFormTwo.updateValueAndValidity();
    let start = moment(this.eventToSave.startDate);
    let end = moment(this.eventToSave.endDate);
    if (
      !this.eventToSaveFormOne.valid || 
      !this.eventToSaveFormTwo.valid ||
      !this.haveAnImage ||
      start.isAfter(end) ||
      this.eventToSave.eventType == ""
    ) {
      return false;
    }
    return true;
  }

  private fillEventData (): void {
    this.eventToSave = { 
      ...this.eventToSave, 
      ...this.eventToSaveFormOne.value,
      guestlist: this.eventToSaveFormTwo.value.guestlist,
      guestlistQuantity: this.eventToSaveFormTwo.value.guestlistQuantity,
      benefits: this.eventToSaveFormTwo.value.benefits,
      warnings: this.eventToSaveFormTwo.value.warnings,
      location: {
        city: this.eventToSaveFormTwo.value.city,
        address: this.eventToSaveFormTwo.value.address
      }
    }
  }

  public fillEventDataToEdit() {
    let selectedEvent: EDEevent = this._eventService.getSelectedEvent();
    if (selectedEvent != null) {
      this.eventToSaveFormOne.controls.name.setValue(selectedEvent.name);
      this.eventToSaveFormOne.controls.musicalGender.setValue(selectedEvent.musicalGender);
      this.eventToSaveFormOne.controls.description.setValue(selectedEvent.description);
      this.eventToSaveFormOne.controls.lineUp.setValue(selectedEvent.lineUp);
      this.eventToSaveFormTwo.controls.city.setValue(selectedEvent.location.city);
      this.eventToSaveFormTwo.controls.address.setValue(selectedEvent.location.address);
      this.eventToSaveFormTwo.controls.benefits.setValue(selectedEvent.benefits);
      this.eventToSaveFormTwo.controls.warnings.setValue(selectedEvent.warnings);
      this.eventToSaveFormTwo.controls.guestlist.setValue(String(selectedEvent.guestlist));
      this.eventToSaveFormTwo.controls.benefits.setValue(selectedEvent.benefits);
      if (selectedEvent.benefits != "") {
        this.eventToSaveFormTwo.get('benefits').disable();
      }
      if (selectedEvent.vjLineUp != "") {
        this.eventToSaveFormOne.controls.hasvj.setValue('1');
        this.eventToSaveFormOne.controls.vjLineUp.setValue(selectedEvent.vjLineUp);
      } else {
        this.eventToSaveFormOne.controls.hasvj.setValue('0');
      }
      if (this.eventToSaveFormTwo.controls.guestlist.value == '1') {
        this.eventToSaveFormTwo.controls.guestlistQuantity.setValue(selectedEvent.guestlistQuantity);
        if (selectedEvent.guestlistUsers.length && selectedEvent.guestlistUsers.length > 0) {
          this.eventToSaveFormTwo.get('guestlist').disable();
          this.eventToSaveFormTwo.controls.guestlistQuantity.disable();
        }
      }
      this.eventToSave._id = selectedEvent._id;
      this.eventToSave.signature = selectedEvent.signature;
      this.eventToSave.status = selectedEvent.status;
      this.eventToSave.createdDate = selectedEvent.createdDate;
      this.eventToSave.imageSrc = selectedEvent.imageSrc;
      this.eventToSave.tickets = selectedEvent.tickets;
      this.eventToSave.eventType = selectedEvent.eventType;
      this.eventToSave.startDate = selectedEvent.startDate;
      this.eventToSave.endDate = selectedEvent.endDate;
      this.temporalImage = selectedEvent.imageSrc;
      this.eventToSave.guestlistUsers = selectedEvent.guestlistUsers;
      this.eventToSave.guestlistQuantity = selectedEvent.guestlistQuantity;
      this.haveAnImage = true;
    } else {
      this._router.navigate(['/events']);
    }
  }

  public selectedTicketDate(date, type, index) {
    this.ticketBatchesDates[index][type] = date;
  }

  public formatDate(date, format) {
    return moment(date).format(format);
  }

  public deleteImage() {
    this.temporalImage = undefined;
    this.imageToUpload = undefined;
  }

}
