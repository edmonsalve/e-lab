import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import { EventService } from '../services/event/event.service';

@Component({
  selector: 'app-ebm',
  templateUrl: './electronic-business-meeting.component.html',
  styleUrls: ['./electronic-business-meeting.component.scss']
})
export class ElectronicBusinessMeetingComponent implements OnInit {

    private readonly EVENT_ID: string = "5d8fae9b20c1873ae75f2284";
    //private readonly EVENT_ID: string = "5d9657f95dc12d0850107562";

    @ViewChild('error') errorAlert: SwalComponent;
    @ViewChild('errorAlready') errorAlreadyAlert: SwalComponent;
    @ViewChild('confirmation') confirmationAlert: SwalComponent;

    public guestlistForm: FormGroup;

    constructor(private fb: FormBuilder, private eventsService: EventService) {}

    ngOnInit() {
        this.guestlistForm = this.fb.group({
            name: ['', Validators.compose([
                Validators.required,
                //Validators.minLength(2),
                //Validators.maxLength(70)
            ])],
            cellphone: ['', Validators.compose([
                Validators.required,
                //Validators.minLength(10),
                //Validators.maxLength(10)
            ])],
            document: ['', Validators.compose([
                Validators.required,
                //Validators.minLength(10),
                //Validators.maxLength(10)
            ])],
            email: ['', Validators.compose([
                Validators.required,
                //Validators.maxLength(70), 
                //Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')
            ])],
            question1: ['', Validators.compose([
                Validators.required,
                //Validators.maxLength(70), 
                //Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')
            ])],
            question2: ['', Validators.compose([
                Validators.required,
                //Validators.maxLength(70), 
                //Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')
            ])]
        })
    }

    public saveUser() {
        if (this.guestlistForm.valid) {
            let userName = this.guestlistForm.value.name,
                userDoc = this.guestlistForm.value.document,
                userEmail = this.guestlistForm.value.email,
                userCellphone = this.guestlistForm.value.cellphone,
                especial = true,
                question1 = this.guestlistForm.value.question1,
                question2 = this.guestlistForm.value.question2;

            this.eventsService.saveUserToGuestlist({ userName, userDoc, userEmail, userCellphone, eventId: this.EVENT_ID, especial, question1, question2 }).subscribe((response: any) => {
                if (response.status == "success") {
                    this.confirmationAlert.show();
                } else if (response.status == "already registered") {
                    this.errorAlreadyAlert.show();
                }
            });
        } else {
            this.errorAlert.show();
        }
    }
}