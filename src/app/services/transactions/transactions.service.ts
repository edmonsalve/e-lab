import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {

  public baseUrl: string = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) { }

  public getTransactionStatus(userId: string, invoice: string) {
    const endpoint = `${this.baseUrl}/payments/transactionStatus`;

    return this.http.get(endpoint, { params: { userId, invoice }});
  }
}
