import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import EDEevent from 'src/app/models/event.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  public selectedEvent: EDEevent;
  public baseUrl: string = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) { }

  public getAll() {
    const endpoint = `${this.baseUrl}/events`;

    return this.http.get(endpoint, { params: { isAdmin: 'true' } });
  }

  public getAllBySignature(signature: string) {
    const endpoint = `${this.baseUrl}/events/${signature}`;

    return this.http.get(endpoint);
  }

  public getEvent(eventId: string) {
    const endpoint = `${this.baseUrl}/events/${eventId}`;

    return this.http.get(endpoint);
  }


  public saveEvent(event: EDEevent) {
    const endpoint = `${this.baseUrl}/events`;

    return this.http.post(endpoint, event);
  }

  public updateEvent(event: EDEevent) {
    const endpoint = `${this.baseUrl}/events`;

    return this.http.put(endpoint, event);
  }

  public deleteEvent(eventId: string) {
    const endpoint = `${this.baseUrl}/events/${eventId}`;

    return this.http.delete(endpoint);
  }

  public activateEvent(eventId: string) {
    const endpoint = `${this.baseUrl}/events/activate`;

    return this.http.post(endpoint, { eventId });
  }

  public getFreeStatistics(eventId: string) {
    const endpoint = `${this.baseUrl}/events/statistics-free/${eventId}`;

    return this.http.get(endpoint);
  }

  public getStatistics(eventId: string) {
    const endpoint = `${this.baseUrl}/events/statistics/${eventId}`;

    return this.http.get(endpoint);
  }

  public setSelectedEvent(event: EDEevent) {
    this.selectedEvent = event;
  }

  public getSelectedEvent(): EDEevent {
    if (this.selectedEvent) {
      return this.selectedEvent;
    } else {
      return null;
    }
  }

  public saveUserToGuestlist(data) {
    const endpoint = `${this.baseUrl}/events/guestlist`;

    return this.http.post(endpoint, data);
  }
}
