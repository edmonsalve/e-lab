import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class JwtInterceptorService implements HttpInterceptor {

  constructor() { }

  intercept(originalRequest: HttpRequest<any>, next: HttpHandler) {

    let loggedUser: any = JSON.parse(localStorage.getItem('loggedUser'));
    if (!loggedUser) {
      return next.handle(originalRequest);
    } else {
      let requestWithJWT = originalRequest.clone({
        setHeaders: { Authorization: `Bearer ${loggedUser.token}` }
      });
      return next.handle(requestWithJWT);
    }
  }
}
