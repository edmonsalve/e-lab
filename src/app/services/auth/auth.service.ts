import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import User from 'src/app/models/user.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public apiUrl: String = environment.apiUrl + '/users';
  private loggedUser: User;

  constructor(private http: HttpClient) { }

  public authenticate(email: string, password: string) {
    return this.http.post(`${this.apiUrl}/login`, { email, password });
  }

  public setLoggedUser(user: User): void {
    this.loggedUser = user;
  }

  public getLoggedUser(): User {
    if (this.loggedUser) return this.loggedUser;
    return JSON.parse(localStorage.getItem('loggedUser')) as User;
  }

  public loggout(): void {
    this.loggedUser = undefined;
    localStorage.clear();
  }

  public isAuthenticated() {
    if (this.loggedUser || localStorage.getItem('loggedUser') != null) return true;
    return false;
  }
}
