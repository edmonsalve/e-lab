import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import User from 'src/app/models/user.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public baseUrl: string = environment.apiUrl;

  constructor(
    private _http: HttpClient
  ) { }

  public getUser(userId: string) {
    const endpoint = `${this.baseUrl}/users/${userId}`;

    return this._http.get(endpoint);
  }

  public getAllUsers() {
    const endpoint = `${this.baseUrl}/users/`;

    return this._http.get(endpoint);
  }

  public getValidatorUsersForEvent(eventId: string) {
    const endpoint = `${this.baseUrl}/users/validators/${eventId}`;

    return this._http.get(endpoint);
  }

  public saveUser(user: User, loggedUserEmail) {
    const endpoint = `${this.baseUrl}/users/signup`;

    return this._http.post(endpoint, {...user, loggedUserEmail});
  }

  public updateUser(user: User) {
    const endpoint = `${this.baseUrl}/users/`;

    return this._http.put(endpoint, user);
  }

  public deleteUser(userId: string) {
    const endpoint = `${this.baseUrl}/users/${userId}`;

    return this._http.delete(endpoint);
  }

  public activateUser(userId: string) {
    const endpoint = `${this.baseUrl}/users/`;

    return this._http.put(endpoint, { userId });
  }

  public validateUser(userId: string, validated: boolean, status: number) {
    const endpoint = `${this.baseUrl}/users/validation`;

    return this._http.post(endpoint, { userId, validated, status });
  }

  public recoveryPassword(userEmail: string) {
    const endpoint = `${this.baseUrl}/users/recoveryPassword`;

    return this._http.post(endpoint, { userEmail });
  }

  public changePassword(data) {
    const endpoint = `${this.baseUrl}/users/changePassword`;

    return this._http.post(endpoint, data);
  }
}
