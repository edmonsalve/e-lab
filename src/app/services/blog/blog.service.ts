import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import Category from '../../models/category.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  private apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  public getMasterCategories(): Promise<Category[]> {
    const endpoint = `${this.apiUrl}/blog/categories`;

    return this.http.get<Category[]>(endpoint).toPromise();
  }

  public getPosts(categoryId: number) {
    const endpoint = `${this.apiUrl}/blog/categories/${categoryId}/posts`;
    
    return this.http.get<Category[]>(endpoint).toPromise();
  }

  public createCategory(data: Category) {
    const endpoint = `${this.apiUrl}/blog/categories`;

    return this.http.post<Category>(endpoint, data);
  }

  public createPost() {}
}
