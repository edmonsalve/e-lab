import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TransactionsService } from '../services/transactions/transactions.service';

declare var window: any;

@Component({
  selector: 'app-payment-status',
  templateUrl: './payment-status.component.html',
  styleUrls: ['./payment-status.component.scss']
})
export class PaymentStatusComponent implements OnInit {

  public userId: string;
  public invoiceReference: string;

  public badParameters: boolean = false;

  public response: any;

  public showLoader = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private transactionService: TransactionsService
  ) { }

  ngOnInit() {
    this.showLoader = true;
    this.activatedRoute.params.subscribe((params) => {
      if (params.userId && params.invoice) {
        this.userId = params.userId;
        this.invoiceReference = params.invoice;
        this.transactionService.getTransactionStatus(this.userId, this.invoiceReference).subscribe((response: any) => {
          this.showLoader = false;
          this.response = response;
        }, (error) => {
          this.showLoader = false;
          this.badParameters = true;
        });
      } else {
        this.showLoader = false;
        this.badParameters = true;
      }
    })
  }

  public closeWindow() {
    window.postMessage("Hello there");
  }

}
