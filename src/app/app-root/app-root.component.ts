import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth/auth.service';
import User from '../models/user.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-app-root',
  templateUrl: './app-root.component.html',
  styleUrls: ['./app-root.component.scss']
})
export class AppRootComponent implements OnInit {

  public loggedUser: User;

  public menuHeight: string = '0.5em';
  public menuOpened: boolean = false;

  constructor(
    private _authService: AuthService,
    private _router: Router
  ) {}

  ngOnInit() {
    this.loggedUser = this._authService.getLoggedUser();
  }

  public toggleMenu() {
    this.menuOpened = !this.menuOpened;
    this.menuHeight = this.menuOpened ? '8em' : '0.5em';
  }

  public logout() {
    this._authService.loggout();
    this._router.navigate(['/']);
  }

  public goToEvents() {
    this._router.navigate(['/events']);
  }

  public goToUsers() {
    this._router.navigate(['/users']);
  }

  public goToChangePassword() {
    this._router.navigate(['/users/changePassword']);
  }

}
