import { Component, OnInit } from '@angular/core';
import EDEevent from "../models/event.model";
import { Router, ActivatedRoute } from '@angular/router';
import { EventService } from '../services/event/event.service';

import * as moment from 'moment';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.scss']
})
export class EventDetailComponent implements OnInit {

  public selectedEvent: EDEevent = new EDEevent();

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _eventService: EventService
  ) {}

  ngOnInit() {
    this.selectedEvent = this._eventService.getSelectedEvent();

    if (!this.selectedEvent) {
      const eventId = this._activatedRoute.snapshot.paramMap.get('id');

      this._eventService.getEvent(eventId).subscribe((event: any) => {
        this.selectedEvent = event.data[0];
      });
    }
  }

  public getFormattedDate(date: Date) {
    return new Date(date).toLocaleDateString();
  }

  public goToEventStatistics() {
    this._router.navigate(['/event-statistics', this.selectedEvent._id ]);
  }

  public goToValidatorsUsers() {
    this._router.navigate([`/event/${this.selectedEvent._id}/validators`]);
  }

}
