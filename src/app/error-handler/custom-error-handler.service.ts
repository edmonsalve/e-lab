import { Injectable, ErrorHandler } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CustomErrorHandlerService implements ErrorHandler {

  handleError(error: any) {
    if (error instanceof HttpErrorResponse) {
      // Backend returns an error
      console.error(error.status, error.message);
    } else {
      // Client side or network error
      console.error('An error ocurred', error);
    }
  }
}
