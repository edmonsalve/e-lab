import { Component, OnInit, EventEmitter, Inject, ViewChild } from '@angular/core';
import User from '../models/user.model';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserService } from '../services/user/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../services/auth/auth.service';
import UserFilters from '../models/userFilters.model';
import { SwalComponent } from '@toverux/ngx-sweetalert2';


@Component({
  selector: 'app-users-panel',
  templateUrl: './users-panel.component.html',
  styleUrls: ['./users-panel.component.scss']
})
export class UsersPanelComponent implements OnInit {

  public users: User[];
  public pageOfItems: User[] = [];
  public userToChange: User;
  public copyUsers: User[];

  public showLoader: boolean = false;
  public loaderMessage: string = "";

  public isAdminUser: boolean = false;
  public eventId: string;

  public loggedUser: any;

  public userFilters: UserFilters = new UserFilters();

  @ViewChild('confirmationActived') confirmationActived: SwalComponent;
  @ViewChild('confirmationInactived') confirmationInactived: SwalComponent;
  @ViewChild('inactiveUserConfirmation') inactiveUserConfirmation: SwalComponent;
  @ViewChild('activeUserConfirmation') activeUserConfirmation: SwalComponent;
  @ViewChild('alreadyExistentUser') alreadyExistentUser: SwalComponent;

  constructor(
    private _matDialog: MatDialog,
    private _userService: UserService,
    private _authService: AuthService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loggedUser = this._authService.getLoggedUser();
    this.loaderMessage = "Consultando usuarios";
    this.showLoader = true;
    this._activatedRoute.params.subscribe((params) => {
      if (params.id) {
        this.isAdminUser = false;
        this.eventId = params.id;
        this.getValidatorUsersForEvent(this.eventId);
      } else {
        this.isAdminUser = true;
        this.getAllUsers();
      }
    });
  }

  public onChangePage(pageOfItems: Array<User>) {
    // update current page of items
    this.pageOfItems = pageOfItems;
  }

  public openCreateUserDialog() {
    const dialogRef = this._matDialog.open(CreateUserDialog, {
      width: '500px',
      data: { isAdmin: this.isAdminUser, eventId: this.eventId }
    });
    const onAddSub = dialogRef.componentInstance.onAddNewUser.subscribe((user: User) => {
      dialogRef.close();
      this.loaderMessage = "Creando el usuario!";
      this.showLoader = true;
      user.status = 2;
      this._userService.saveUser(user, this.loggedUser.email).subscribe((response: any) => {
        if (response.status == "success") {
          this.users.push(user);
          setTimeout(() => {
            this.showLoader = false;
          }, 2000)
        } else if (response.status == "An error ocurred") {
          this.alreadyExistentUser.show();
        }
      });
    });
  }

  public openUpdateUserDialog(user: User) {
    const dialogRef = this._matDialog.open(CreateUserDialog, {
      width: '500px',
      data: { isAdmin: true, isEditMode: true, user }
    });
    const onAddSub = dialogRef.componentInstance.onAddNewUser.subscribe((user: User) => {
      dialogRef.close();
      this.loaderMessage = "Actualizando el usuario";
      this.showLoader = true;
      this._userService.updateUser(user).subscribe((response) => {
        this.users.map((userMapped) => {
          if (userMapped._id == user._id) {
            userMapped = user;
          }
        });
        setTimeout(() => {
          this.showLoader = false;
        }, 2000)
      });
    });
  }

  public openActivateUserConfirmation(user) {
    this.userToChange = user;
    this.activeUserConfirmation.show();
  }

  public openInactiveUserConfirmation(user) {
    this.userToChange = user;
    this.inactiveUserConfirmation.show();
  }

  public activateUser() {
    this.loaderMessage = "Activando usuario";
    this.showLoader = true;
    this._userService.activateUser(this.userToChange._id).subscribe((response) => {
      this.users.map((user) => {
        if (user._id == this.userToChange._id) {
          user.status = 2;
        }
      })
      setTimeout(() => {
        this.showLoader = false;
        this.confirmationActived.show();
      }, 2000)
    });
  }

  
  public deleteUser() {
    this.loaderMessage = "Inactivando usuario";
    this.showLoader = true;
    this._userService.deleteUser(this.userToChange._id).subscribe((response) => {
      this.users.map((user) => {
        if (user._id == this.userToChange._id) {
          user.status = 0;
        }
      })
      setTimeout(() => {
        this.showLoader = false;
        this.confirmationInactived.show();
      }, 2000)
    });
  }

  private getAllUsers() {
    this._userService.getAllUsers().subscribe((response: any) => {
      this.users = response.users;
      this.copyUsers = this.users;
      this.showLoader = false;
    }, (error) => {
      throw(error);
    });
  }

  private getValidatorUsersForEvent(eventId: string) {
    this._userService.getValidatorUsersForEvent(eventId).subscribe((response: any) => {
      this.users = response.users;
      this.showLoader = false;
    }, (error) => {
      throw(error);
    });
  }

  public filterUsers() {
    if (this.userFilters.name == "" && this.userFilters.city == "" && this.userFilters.role == "" && this.userFilters.signature == "" && this.userFilters.status == 3) {
      this.users = this.copyUsers;
    } else {
      let result = [];
      this.users.map((userMapped) => {
        // Filtro por todos los campos
        if (this.userFilters.name != "" && this.userFilters.city != "" && this.userFilters.signature != ""  && this.userFilters.status != 3
        && this.userFilters.role != "") {

          if (userMapped.name.toLowerCase().includes(this.userFilters.name.toLowerCase()) && userMapped.location == this.userFilters.city
          && userMapped.signature && userMapped.signature.toLowerCase().includes(this.userFilters.signature.toLowerCase()) && userMapped.status == this.userFilters.status && userMapped.role == this.userFilters.role) {

            result.push(userMapped);
          }

        } else if (this.userFilters.name != "" && this.userFilters.city != "" && this.userFilters.signature != "" &&  this.userFilters.status != 3
        && this.userFilters.role == "") {

          if (userMapped.name.toLowerCase().includes(this.userFilters.name.toLowerCase()) && userMapped.location == this.userFilters.city
          && userMapped.signature && userMapped.signature.toLowerCase().includes(this.userFilters.signature.toLowerCase()) && userMapped.status == this.userFilters.status) {

            result.push(userMapped);
          }

        } else if (this.userFilters.name != "" && this.userFilters.city != "" && this.userFilters.signature != "" && this.userFilters.status == 3
        && this.userFilters.role == "") {

          if (userMapped.name.toLowerCase().includes(this.userFilters.name.toLowerCase()) && userMapped.location == this.userFilters.city
          && userMapped.signature && userMapped.signature.toLowerCase().includes(this.userFilters.signature.toLowerCase())) {

            result.push(userMapped);
          }

        } else if (this.userFilters.name != "" && this.userFilters.city != "" && this.userFilters.signature == "" && this.userFilters.status == 3
        && this.userFilters.role == "") {

          if (userMapped.name.toLowerCase().includes(this.userFilters.name.toLowerCase()) && userMapped.location == this.userFilters.city) {

            result.push(userMapped);
          }

        } else if (this.userFilters.name != "" && this.userFilters.city == "" && this.userFilters.signature == "" && this.userFilters.status == 3
        && this.userFilters.role == "") {

          if (userMapped.name.toLowerCase().includes(this.userFilters.name.toLowerCase())) {

            result.push(userMapped);
          }

        } else if (this.userFilters.name == "" && this.userFilters.city != "" && this.userFilters.signature == "" && this.userFilters.status == 3
        && this.userFilters.role == "") {

          if (userMapped.location.toLowerCase().includes(this.userFilters.city.toLowerCase())) {

            result.push(userMapped);
          }

        } else if (this.userFilters.name == "" && this.userFilters.city == "" && this.userFilters.signature != "" && this.userFilters.status == 3
        && this.userFilters.role == "") {

          if (userMapped.signature && userMapped.signature.toLowerCase().includes(this.userFilters.signature.toLowerCase())) {

            result.push(userMapped);
          }

        } else if (this.userFilters.name == "" && this.userFilters.city == "" && this.userFilters.signature == "" && this.userFilters.status != 3
        && this.userFilters.role == "") {

          if (userMapped.status == this.userFilters.status) {

            result.push(userMapped);
          }

        } else if (this.userFilters.name == "" && this.userFilters.city == "" && this.userFilters.signature == "" && this.userFilters.status == 3
        && this.userFilters.role != "") {

          if (userMapped.role == this.userFilters.role) {

            result.push(userMapped);
          }

        } else if (this.userFilters.name != "" && this.userFilters.city != "" && this.userFilters.signature == "" && this.userFilters.status == 3
        && this.userFilters.role == "") {

          if (userMapped.location == this.userFilters.city && userMapped.name.toLowerCase().includes(this.userFilters.name.toLowerCase())) {

            result.push(userMapped);
          }

        } else if (this.userFilters.name == "" && this.userFilters.city == "" && this.userFilters.signature == "" && this.userFilters.status != 3
        && this.userFilters.role != "") {

          if (userMapped.role == this.userFilters.role && userMapped.status == this.userFilters.status) {

            result.push(userMapped);
          }

        }
      });
      this.showLoader = true;
      if(result.length > 0) {
        this.users = result;
      } else {
        this.users = [];
      }
      this.showLoader = false;
    }
  }

  public clearFilters() {
    this.userFilters = new UserFilters();
    this.users = this.copyUsers;
  }

  public logout() {
    this._authService.loggout();
    this._router.navigate(['/']);
  }

  public goToHome() {
    this._router.navigate(['/home']);
  }

  public goToEvents() {
    this._router.navigate(['/events']);
  }

  public goToUsers() {
    this._router.navigate(['/users']);
  }
}


@Component({
  selector: 'create-user-dialog',
  templateUrl: 'create-user-dialog.html',
  styleUrls: ['create-user-dialog.scss']
})
export class CreateUserDialog {

  public user: User = new User();
  public userOnEdition: User;
  public onAddNewUser = new EventEmitter();

  public isAdminUser;
  public isEditMode: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<CreateUserDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.isAdminUser = data.isAdmin;
    this.isEditMode = data.isEditMode;
    if (this.isEditMode) {
      this.user = data.user;
      console.log(data.user)
    }
  }


  onConfirmClick() {
    if (!this.isAdminUser) {
      this.user.role = 'Portero';
      this.user.eventId = this.data.eventId;
    }
    this.onAddNewUser.emit(this.user);
  }

  onNoClick() {
    this.dialogRef.close();
  }
}