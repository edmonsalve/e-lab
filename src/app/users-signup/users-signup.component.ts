import { Component, OnInit, EventEmitter, Inject, ViewChild } from '@angular/core';
import User from '../models/user.model';
import { UserService } from '../services/user/user.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import { AuthService } from '../services/auth/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'terms-conditions-dialog',
  templateUrl: 'terms-conditions-dialog.html',
  styleUrls: ['terms-conditions-dialog.scss']
})
export class TermsAndConditionsDialog {

  public onConfirmEvent = new EventEmitter();

  constructor(
    public dialogRef: MatDialogRef<TermsAndConditionsDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}


  onConfirmClick() {
    this.onConfirmEvent.emit(true);
  }

  onNoClick() {
    this.dialogRef.close();
  }
}


@Component({
  selector: 'app-users-signup',
  templateUrl: './users-signup.component.html',
  styleUrls: ['./users-signup.component.scss']
})
export class UsersSignupComponent implements OnInit {

  @ViewChild('confirmation') swalConfirmation: SwalComponent;
  @ViewChild('errorSwalPasswords') errorSwalPasswords: SwalComponent;
  @ViewChild('errorSwalData') errorSwalData: SwalComponent;
  @ViewChild('errorSwalTerms') errorSwalDataTerms: SwalComponent;
  @ViewChild('errorDuplicatedUser') errorDuplicatedUser: SwalComponent;

  public user: User = new User();
  public confirmPassword: string = "";

  public conditions: boolean = false;
  public showLoader: boolean = false;

  public userSignUpForm: FormGroup;

  constructor(
    private userService: UserService,
    private _matDialog: MatDialog,
    private _router: Router,
    private _authService: AuthService,
    private _formBuilder: FormBuilder
  ) {
    this.userSignUpForm = this._formBuilder.group({
      name: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', Validators.compose([Validators.maxLength(70), Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'), Validators.required])],
      location: ['', Validators.required],
      password: ['', Validators.required],
      passwordConfirmation: ['', Validators.required],
      conditions: [false, Validators.required],
      cellPhone: ['', Validators.compose([
        Validators.required,
        Validators.minLength(10)
      ])],
      age: ['', Validators.required],
      gender: ['', Validators.required],
      expYears: ['', Validators.required],
      eventsLastYear: ['', Validators.required],
      signature: ['', Validators.required],
      clubOrOrganizer: ['', Validators.required],
      socialLink1: ['', Validators.required],
      socialLink2: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  public signupUser() {
    this.userSignUpForm.controls.email.setValue(this.userSignUpForm.controls.email.value.trim());
    this.userSignUpForm.updateValueAndValidity();
    if (this.userSignUpForm.controls.conditions.value) {
      if (this.userSignUpForm.valid) {
        this.user = this.userSignUpForm.value;
        if (this.user.password === this.userSignUpForm.value.passwordConfirmation) {
          this.showLoader = true;
          this.user.role = 'Firma';
          this.user.status = 1;
          this.userService.saveUser(this.user, null).subscribe((response: any) => {
            if (response.status == "success") {
              this._authService.setLoggedUser(response['data']);
              this.user = new User();
              this.showLoader = false;
              this.swalConfirmation.show();
            } else {
              this.showLoader = false;
              this.errorDuplicatedUser.show();
            }
          }, (error) => {
            console.error(error);
            this.showLoader = false;
          });
        } else {
          this.errorSwalPasswords.show();
        }
      } else {
        this.errorSwalData.show();
      }
    } else {
      this.errorSwalDataTerms.show();
    }
    
  }

  public openTermsDialog() {
    const dialogRef = this._matDialog.open(TermsAndConditionsDialog, {
      width: '500px',
    });
  }

  public goToHome() {
    this._router.navigate(['/home']);
  }

  public goToLogin() {
    this._router.navigate(['/']);
  }
}


