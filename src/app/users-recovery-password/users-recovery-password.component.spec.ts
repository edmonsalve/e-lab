import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersRecoveryPasswordComponent } from './users-recovery-password.component';

describe('UsersRecoveryPasswordComponent', () => {
  let component: UsersRecoveryPasswordComponent;
  let fixture: ComponentFixture<UsersRecoveryPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersRecoveryPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersRecoveryPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
