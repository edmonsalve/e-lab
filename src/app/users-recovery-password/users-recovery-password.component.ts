import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import { UserService } from '../services/user/user.service';

@Component({
  selector: 'app-users-recovery-password',
  templateUrl: './users-recovery-password.component.html',
  styleUrls: ['./users-recovery-password.component.scss']
})
export class UsersRecoveryPasswordComponent implements OnInit {

  public userEmail: string = "";

  public showLoader = false;

  @ViewChild('confirmation') swalConfirmation: SwalComponent
  @ViewChild('error') swalError: SwalComponent
  constructor(
    private _router: Router,
    private _userService: UserService
  ) { }

  ngOnInit() {
  }

  public goToLogin() {
    this._router.navigate(['/']);
  }

  public recoveryPassword() {
    if (this.validateEmail(this.userEmail)) {
      this.showLoader = true;
      this._userService.recoveryPassword(this.userEmail).subscribe((response) => {
        this.showLoader = false;
        this.swalConfirmation.show();
      })
    } else {
      this.swalError.show();
    }
  }

  public validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

}
