import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { EventService } from '../services/event/event.service';
import EDEevent from '../models/event.model';
import EventFilters from '../models/eventFilters.model';
import { AuthService } from './../services/auth/auth.service';
import * as moment from 'moment';
import User from '../models/user.model';
import { SwalComponent } from '@toverux/ngx-sweetalert2';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {

  public loggedUser: User;
  public events: EDEevent[] = [];
  public copyEvents: EDEevent[];
  public pageOfItems: EDEevent[] = [];
  public eventFilters: EventFilters = new EventFilters();
  public eventToUpdate: string = "";

  public menuHeight: string = '0.5em';
  public menuOpened: boolean = false;
  public showLoader: boolean = false;

  public datetimePickerConfig = {
    bigBanner: true,
    timePicker: false,
    format: 'dd-MM-yyyy',
    defaultOpen: false
  };

  @ViewChild('confirmation') confirmToDelete: SwalComponent;
  @ViewChild('confirmationToActive') confirmToActive: SwalComponent;
  @ViewChild('confirmInactived') confirmInactived: SwalComponent;
  @ViewChild('failedToInactive') failedToInactive: SwalComponent;
  @ViewChild('confirmActived') confirmActived: SwalComponent;

  constructor(
    private _router: Router,
    private _eventService: EventService,
    public _authService: AuthService
  ) { }

  ngOnInit() {
    this.loggedUser = this._authService.getLoggedUser();
    if (this.loggedUser.role == 'Administrador') {
      this._eventService.getAll().subscribe((response: any) => {
        this.events = response.data;
        this.copyEvents = this.events;
      });
    } else {
      this._eventService.getAllBySignature(this.loggedUser._id).subscribe((response: any) => {
        this.events = response.data;
        this.copyEvents = this.events;
      });
    }
  }

  public filter() {
    let result: EDEevent[] = [];

    if (!this.eventFilters.createdAt && this.eventFilters.signature == "" && !this.eventFilters.startDate && this.eventFilters.state == "" && this.eventFilters.title == "") {
      if (this.copyEvents) {
        this.events = this.copyEvents;
      }
      return;
    }
    this.events.map((eventMapped) => {

      // FILtro por todos los campos
      if (this.eventFilters.createdAt && this.eventFilters.signature !== "" && this.eventFilters.state !== ''
        && this.eventFilters.title !== "" && this.eventFilters.startDate) {

        if (eventMapped.name.toLowerCase().includes(this.eventFilters.title) && moment(eventMapped.createdDate).isSame(this.eventFilters.createdAt, 'd')
          && moment(eventMapped.startDate).isSame(this.eventFilters.startDate, 'd') && eventMapped.signature.includes(this.eventFilters.signature)
          && eventMapped.status == this.eventFilters.state) {

          result.push(eventMapped);

        }
        // Filtro por nombre, organizador, estado y fecha de inicio
      } else if (!this.eventFilters.createdAt && this.eventFilters.signature !== "" && this.eventFilters.state !== ''
        && this.eventFilters.title !== "" && this.eventFilters.startDate) {

        if (eventMapped.name.toLowerCase().includes(this.eventFilters.title) && moment(eventMapped.startDate).isSame(this.eventFilters.startDate, 'd') && eventMapped.signature.includes(this.eventFilters.signature)
          && eventMapped.status == this.eventFilters.state) {

          result.push(eventMapped);

        }
        // Filtro por nombre, estado y fecha de inicio
      } else if (!this.eventFilters.createdAt && this.eventFilters.signature == "" && this.eventFilters.state !== ''
        && this.eventFilters.title !== "" && this.eventFilters.startDate) {

        if (eventMapped.name.toLowerCase().includes(this.eventFilters.title) && moment(eventMapped.startDate).isSame(this.eventFilters.startDate, 'd') && eventMapped.status == this.eventFilters.state) {

          result.push(eventMapped);

        }
        // Filtro por estado y fecha de inicio
      } else if (!this.eventFilters.createdAt && this.eventFilters.signature == "" && this.eventFilters.state !== ''
        && this.eventFilters.title == "" && this.eventFilters.startDate) {

        let selected = moment(this.eventFilters.startDate).format('YYYY-MM-DD');
        let existent = moment(eventMapped.startDate).format('YYYY-MM-DD');
        if (moment(existent).isSame(selected, 'd') && eventMapped.status == this.eventFilters.state) {

          result.push(eventMapped);

        }
        // Filtro por nombre y estado
      } else if (!this.eventFilters.createdAt && this.eventFilters.signature == "" && this.eventFilters.state !== ''
        && this.eventFilters.title !== "" && !this.eventFilters.startDate) {

        if (eventMapped.name.toLowerCase().includes(this.eventFilters.title.toLowerCase()) && eventMapped.status == this.eventFilters.state) {

          result.push(eventMapped);

        }
        // Filtro por fecha de creación y estado
      } else if (this.eventFilters.createdAt && this.eventFilters.signature == "" && this.eventFilters.state !== ''
        && this.eventFilters.title == "" && !this.eventFilters.startDate) {

        let selected = moment(this.eventFilters.createdAt).format('YYYY-MM-DD');
        let existent = moment(eventMapped.createdDate).format('YYYY-MM-DD');
        if (moment(selected).isSame(existent, 'd') && eventMapped.status == this.eventFilters.state) {

          result.push(eventMapped);

        }
        // Filtro por organizador y estado
      } else if (!this.eventFilters.createdAt && this.eventFilters.signature !== "" && this.eventFilters.state != ''
        && this.eventFilters.title == "" && !this.eventFilters.startDate) {

        if (eventMapped.signature.toLowerCase().includes(this.eventFilters.signature.toLowerCase()) && eventMapped.status == this.eventFilters.state) {

          result.push(eventMapped);

        }
        // Filtro por estado
      } else if (!this.eventFilters.createdAt && this.eventFilters.signature == "" && this.eventFilters.state != ''
        && this.eventFilters.title == "" && !this.eventFilters.startDate) {

        if (eventMapped.status == this.eventFilters.state) {

          result.push(eventMapped);

        }
        // Filtro solo por nombre
      } else if (!this.eventFilters.createdAt && this.eventFilters.signature == "" && this.eventFilters.state == ''
        && this.eventFilters.title != "" && !this.eventFilters.startDate) {

        if (eventMapped.name.toLowerCase().includes(this.eventFilters.title)) {

          result.push(eventMapped);

        }
        // Filtro solo por organizador
      } else if (!this.eventFilters.createdAt && this.eventFilters.signature != "" && this.eventFilters.state == ''
        && this.eventFilters.title == "" && !this.eventFilters.startDate) {

        if (eventMapped.signature.toLowerCase().includes(this.eventFilters.signature)) {

          result.push(eventMapped);

        }
        // Filtro solo por fecha de realizacion
      } else if (!this.eventFilters.createdAt && this.eventFilters.signature == "" && this.eventFilters.state == ''
        && this.eventFilters.title == "" && this.eventFilters.startDate) {

        let selected = moment(this.eventFilters.startDate);
        let existent = moment(eventMapped.startDate);
        if (moment(existent).isSame(selected, 'd')) {

          result.push(eventMapped);

        }
        // Filtro por fecha de realizacion y organizador
      } else if (!this.eventFilters.createdAt && this.eventFilters.signature != "" && this.eventFilters.state == ''
        && this.eventFilters.title == "" && this.eventFilters.startDate) {

        let selected = moment(this.eventFilters.startDate);
        let existent = moment(eventMapped.startDate);
        if (moment(existent).isSame(selected, 'd') && eventMapped.signature.toLowerCase().includes(this.eventFilters.signature.toLowerCase())) {

          result.push(eventMapped);

        }
        // Filtro por nombre y organizador
      } else if (!this.eventFilters.createdAt && this.eventFilters.signature != "" && this.eventFilters.state == ''
        && this.eventFilters.title != "" && !this.eventFilters.startDate) {

        if (eventMapped.signature.toLowerCase().includes(this.eventFilters.signature) && eventMapped.name.toLowerCase().includes(this.eventFilters.title.toLowerCase())) {

          result.push(eventMapped);

        }
      }
    });
    this.showLoader = true;
    if (result.length > 0) {
      this.events = result;
    } else {
      this.events = [];
    }
    this.showLoader = false;
  }

  public onChangePage(pageOfItems: Array<EDEevent>) {
    // update current page of items
    this.pageOfItems = pageOfItems;
  }

  public showDeleteConfirmation(eventId) {
    this.confirmToDelete.show();
    this.eventToUpdate = eventId;
  }

  public showActiveConfirmation(eventId) {
    this.confirmToActive.show();
    this.eventToUpdate = eventId;
  }

  public inactiveEvent() {
    this._eventService.deleteEvent(this.eventToUpdate).subscribe((response: any) => {
      if (response.status == 'success') {
        this.confirmInactived.show();
        this.events = this.events.map((event) => {
          if (event._id == this.eventToUpdate) event.status = 'inactive';
          return event;
        });
      } else if (response.status == 'An error ocurred') {
        this.failedToInactive.show();
      }
    });
  }

  public activateEvent() {
    this._eventService.activateEvent(this.eventToUpdate).subscribe((response: any) => {
      this.confirmActived.show();
      this.events = this.events.map((event) => {
        if (event._id == this.eventToUpdate) event.status = 'active';
        return event;
      });
    });
  }

  public onDateSelected(event) {
    console.log(event);
  }

  public clearFilters() {
    this.eventFilters = new EventFilters();
    this.events = this.copyEvents;
  }

  public goToHome() {
    this._router.navigate(['/home']);
  }

  public goToDetail(event: EDEevent) {
    this._eventService.setSelectedEvent(event);
    this._router.navigate([`/event-detail/${event._id}`]);
  }

  public goToCreateEvent() {
    this._router.navigate(['/create-event']);
  }

  public goToEditEvent(event) {
    this._eventService.setSelectedEvent(event);
    this._router.navigate(['/update-event']);
  }

  public logout() {
    this._authService.loggout();
    this._router.navigate(['/']);
  }

  public goToEvents() {
    this._router.navigate(['/events']);
  }

  public goToUsers() {
    this._router.navigate(['/users']);
  }
}
