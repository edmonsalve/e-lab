import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppRootComponent } from './app-root/app-root.component';
import { CreateEventComponent } from './create-event/create-event.component';
import { EventsComponent } from './events/events.component';
import { LoginComponent } from './login/login.component';
import { BlogMasterComponent } from './blog-master/blog-master.component';
import { PostsListComponent } from './blog-master/posts-list/posts-list.component';
import { UsersPanelComponent } from "./users-panel/users-panel.component";
import { UsersSignupComponent } from './users-signup/users-signup.component';
import { AuthGuardService } from './guards/auth.guard';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { EventStatisticsComponent } from './event-statistics/event-statistics.component';
import { UserValidationComponent } from './user-validation/user-validation.component';
import { UsersRecoveryPasswordComponent } from './users-recovery-password/users-recovery-password.component';
import { UsersChangePasswordComponent } from './users-change-password/users-change-password.component';
import { PaymentStatusComponent } from './payment-status/payment-status.component';
import { OrientesCrowdComponent } from './orientes-crowd/orientes-crowd.component';
import { ElectronicBusinessMeetingComponent } from './electronic-business-meeting/electronic-business-meeting.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'signup',
    component: UsersSignupComponent
  },
  {
    path: 'blog',
    component: BlogMasterComponent
  },
  {
    path: 'home',
    component: AppRootComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'create-event',
    component: CreateEventComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'update-event',
    component: CreateEventComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'events',
    component: EventsComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'event-detail/:id',
    component: EventDetailComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'event/:id/validators',
    component: UsersPanelComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'event-statistics/:id',
    component: EventStatisticsComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'posts',
    component: PostsListComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'users',
    component: UsersPanelComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'users/validation/:id',
    component: UserValidationComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'users/recoveryPassword',
    component: UsersRecoveryPasswordComponent
  },
  {
    path: 'users/changePassword',
    component: UsersChangePasswordComponent
  },
  {
    path: 'paymentResponse/:invoice/:userId',
    component: PaymentStatusComponent
  },
  {
    path: 'electronic-business-meeting',
    component: ElectronicBusinessMeetingComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
