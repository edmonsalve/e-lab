import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import { EventService } from '../services/event/event.service';

@Component({
  selector: 'app-orientes-crowd',
  templateUrl: './orientes-crowd.component.html',
  styleUrls: ['./orientes-crowd.component.scss']
})
export class OrientesCrowdComponent implements OnInit {

    private readonly EVENT_ID: string = "5d67d1cf7b54e87d6fa52462";

    @ViewChild('error') errorAlert: SwalComponent;
    @ViewChild('errorAlready') errorAlreadyAlert: SwalComponent;
    @ViewChild('confirmation') confirmationAlert: SwalComponent;

    public guestlistForm: FormGroup;

    constructor(private fb: FormBuilder, private eventsService: EventService) {}

    ngOnInit() {
        this.guestlistForm = this.fb.group({
            name: ['', Validators.compose([
                Validators.required,
                //Validators.minLength(2),
                //Validators.maxLength(70)
            ])],
            document: ['', Validators.compose([
                Validators.required,
                //Validators.minLength(7),
                //Validators.maxLength(15)
            ])],
            cellphone: ['', Validators.compose([
                Validators.required,
                //Validators.minLength(10),
                //Validators.maxLength(10)
            ])],
            email: ['', Validators.compose([
                Validators.required,
                //Validators.maxLength(70), 
                //Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')
            ])]
        })
    }

    public saveUser() {
        if (this.guestlistForm.valid) {
            let userName = this.guestlistForm.value.name,
                userDoc = this.guestlistForm.value.document,
                userEmail = this.guestlistForm.value.email,
                userCellphone = this.guestlistForm.value.cellphone;

            this.eventsService.saveUserToGuestlist({ userName, userDoc, userEmail, userCellphone, eventId: this.EVENT_ID }).subscribe((response: any) => {
                if (response.status == "success") {
                    this.confirmationAlert.show();
                } else if (response.status == "already registered") {
                    this.errorAlreadyAlert.show();
                }
            });
        } else {
            this.errorAlert.show();
        }
    }
}