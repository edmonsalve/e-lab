import { Component, OnInit, EventEmitter, Inject, ViewChild } from '@angular/core';
import { AuthService } from '../services/auth/auth.service';
import { Router } from '@angular/router';
import User from '../models/user.model';
import { SwalComponent } from '@toverux/ngx-sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public email: string = '';
  public password: string = '';
  public showLoader: boolean = false;

  @ViewChild('errorSwal') errorSwal: SwalComponent;
  @ViewChild('errorSwalValidation') errorSwalValidation: SwalComponent;
  @ViewChild('errorNoAuthValidation') errorNoAuthValidation: SwalComponent;
  
  constructor(
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
  }

  public authenticate() {
    this.showLoader = true;
    this.email = this.email.toLowerCase();
    this.authService.authenticate(this.email, this.password).subscribe((response) => {
      let user = response['data'];
      if (user.role == "Administrador" || user.role == "Firma") {
        if (user.status == 2 || user.status == 3) {
          this.authService.setLoggedUser(response['data'] as User);
          localStorage.setItem('loggedUser', JSON.stringify(response['data']));
          setTimeout(() => {
            this.router.navigate(['/home']);
          }, 2000);
        } else if (user.status == 1 || user.status == null) {
          this.errorSwalValidation.show();
          this.showLoader = false;
        }
      } else {
        this.errorNoAuthValidation.show();
        this.showLoader = false;
      }
    }, (error) => {
      this.errorSwal.show();
      this.showLoader = false;
      throw(error);
    });
  }

  public signup() {
    this.router.navigate(['/signup']);
  }

  public goToRecovery() {
    this.router.navigate(['/users/recoveryPassword']);
  }

}