import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { AuthService } from "../services/auth/auth.service";

@Injectable({
    providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor (
      private _authService: AuthService, 
      public router: Router
  ) {}

  canActivate(): boolean {
    if (!this._authService.isAuthenticated()) {
      this.router.navigate(['']);
      return false;
    }
    return true;
  }
}